#-*- coding: utf-8 -*-

import re
import md5

from django.conf import settings

def encode_options(options):
    options_enc = '{ '
    
    for option_key, option_value in options.items():
        options_enc += "%s: " % option_key 
        
        if isinstance(option_value, str):
            options_enc += "'%s'" % option_value
        elif isinstance(option_value, int) or isinstance(option_value, float):
            options_enc += "%s" % option_value
        elif isinstance(option_value, type(re.compile(''))):
            options_enc += "/%s/" % option_value.pattern
        else:
            raise TypeError("Unsupported type %s." % type(option_value))

        options_enc += ", "
        
    if len(options_enc) > 2:
        options_enc = options_enc[:-2]
        
    options_enc += ' }'
    
    return options_enc

def get_key(app, model, field):
    key = md5.new()
    key.update(app)
    key.update(model)
    key.update(field)
    key.update(settings.SECRET_KEY)
    return key.hexdigest()
