from django.conf.urls.defaults import *

urlpatterns = patterns('',
    url(r'(?P<key>\w*)/$', 'multiautocomplete.views.ajax_search_view', name='multiautocomplete'),
)
