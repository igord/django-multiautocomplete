#-*- coding:utf-8 -*-
'''
Created on Oct 24, 2011

@author: igor

Rebuilding cache for multi_autocomplete widget.
This script should be run periodicaly with e.g. cron.
'''

import sys
import os

# Setting path to the project dir
sys.path.append(os.path.abspath("%s/../" % \
    os.path.dirname(os.path.abspath(__file__))))

from django.core.management import setup_environ
import settings
settings.DEBUG = False
setup_environ(settings)


from django.db.models.loading import get_model
from multiautocomplete.models import MultiAutocompleteCache


def cache_rebuild():

    # Delete old cache
    MultiAutocompleteCache.objects.all().delete()
    
    # Find all applications and models used in multi_autocomplete
    apps = {}
    for field in settings.MULTI_AUTOCOMPLETE_FIELDS:
        app = apps.setdefault(field[0], set())
        app.add(field[1])
        
    # Cache rebuilding
    for app_name, models in apps.items():
        for model_name in models:
            model = get_model(app_name, model_name)
            print model
            for obj in model.objects.all():
                MultiAutocompleteCache.objects.update_cache(obj)
            
if __name__ == '__main__':
    cache_rebuild()



    

    
