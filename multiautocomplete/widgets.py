 
# -*- coding: utf-8 -*-
# MultiStringAutoComplete django widget
# Uses jquery autocomplete plugin, version 1.1.3, 
# from http://www.devbridge.com/projects/autocomplete/jquery/
#
# Author: Igor R. Dejanovic, igor.dejanovic@gmail.com


import re

from django import forms
from django.core.urlresolvers import reverse
from django.forms.widgets import flatatt
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.conf import settings

from utils import encode_options, get_key

class MultiAutoComplete(forms.TextInput):

    class Media:
        if settings.DEBUG:
            js = (  #settings.MEDIA_URL + "multiautocomplete/jquery-1.3.2.min.js",
#                    settings.MEDIA_URL + "jquery/ui/jquery.ui.core.js",
#                    settings.MEDIA_URL + "jquery/ui/jquery.ui.widget.js",
                    settings.STATIC_URL + "multiautocomplete/jquery.autocomplete.js",
            )
        else:
            js = (  # settings.MEDIA_URL + "multiautocomplete/jquery-1.3.2.min.js",
#                    settings.MEDIA_URL + "jquery/ui/minified/jquery.ui.core.min.js",
#                    settings.MEDIA_URL + "jquery/ui/minified/jquery.ui.widget.min.js",
                    settings.STATIC_URL + "multiautocomplete/jquery.autocomplete-min.js",
            )
            
        # Styling
        css = { 'all':
            (
#             settings.MEDIA_URL + "jquery/themes/current/jquery.ui.core.css",
#             settings.MEDIA_URL + "jquery/themes/current/jquery.ui.theme.css",
#             settings.MEDIA_URL + "jquery/themes/current/jquery.ui.autocomplete.css",
             settings.STATIC_URL + "multiautocomplete/styles.css", )
        }

    def __init__(self, field_fqn, source=None, context=None, options={}, attrs={}):
        """
        @source     - url of the ajax view
        @field_fqn  - fully qualified name of the field in the form 'app:model:field'. Lowercase.
                        Used to calculate cache unique ID.
        @source     - url for ajax call
        @context    - integer representing context of this field.
        """
        self.attrs = attrs
        
        key = get_key(*field_fqn.split(':'))
        if not source:
            self.source = reverse('multiautocomplete', args=(key,))
        else:
            self.source = source
        
        
        self.options = {        
            'delimiter': re.compile(r'\n'), # Delimiter will be newline by default
            'minChars': 1,
            'serviceUrl': self.source
        }
        self.options.update(options)
        
        if context:
            self.options['context'] = context
        
        if self.source:
            if isinstance(self.source, str):
                self.options.update({'serviceUrl': str(escape(self.source))})
            else:
                raise ValueError('source type is not valid')
            
        if len(self.options) > 0:
            self.options = encode_options(self.options)
            
        self.attrs.update({
            'class': 'vLargeTextField',
            'rows': 10,
            'cols': 40
        })
        
    def render_js(self, field_id):        
        return u'''django.jQuery('#%(field_id)s').autocomplete(%(options)s);
            ''' % {
                'field_id': field_id,
                'options': self.options,
            }

    def render(self, name, value=None, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)

        if not self.attrs.has_key('id'):
            final_attrs['id'] = 'id_%s' % name

        js = u'''<script type="text/javascript"><!--//
                %s//--></script>''' % self.render_js(final_attrs['id'])
         
        return mark_safe(u'''<textarea type="text" %(attrs)s>%(value)s</textarea>
                        %(js)s
                        ''' % {
                            'name': name,
                            'value': value if value else "",
                            'attrs' : flatatt(final_attrs),
                            'js' : js,
                        })


    